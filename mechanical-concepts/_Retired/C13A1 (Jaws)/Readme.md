# CONCEPT: C13A1 (JAWS)
Status: Retired

---

## Team Lead(s):
|Name|GitLab|
|---|---|
| Dugan Karnazes |  |
| | |

## Concept Overview:

### Introduction

### Function

### Mechanical Design

### Electrical

### Manufacturability

### Aesthetic

### Known Problems / Considerations

## Issue Labels:
~"C13A1/JAWS"

## Related Issues and Boards:

## Other Important Links
