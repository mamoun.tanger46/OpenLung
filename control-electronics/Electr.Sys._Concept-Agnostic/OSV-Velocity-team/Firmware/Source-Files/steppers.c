/**
 * steppers.c
 *
 * Author: Corbin Bremmeyr
 * Date: 9 April 2020
 *
 * Functions to interact with the stepper motor driver chips
 */

#include <Arduino.h>
#include "steppers.h"

// Pin assignments
// TODO: replace with correct values when they are known
#define DIR_PIN_1 (1)
#define DIR_PIN_2 (1)
#define STEP_PIN_1 (1)
#define STEP_PIN_2 (1)

// Pulse length of step pulse (units: us)
#define STEP_PULSE_LEN (3)

/**
 * Prepare the stepper library for use
 */
void step_init(void) {

    // Set pin modes
    pinMode(DIR_PIN_1, OUTPUT);
    pinMode(DIR_PIN_2, OUTPUT);
    pinMode(STEP_PIN_1, OUTPUT);
    pinMode(STEP_PIN_2, OUTPUT);

    // Set pin inital values
    digitalWrite(DIR_PIN_1, LOW);
    digitalWrite(DIR_PIN_2, LOW);
    digitalWrite(STEP_PIN_1, LOW);
    digitalWrite(STEP_PIN_2, LOW);
}

/**
 * Move both the stepper motors in sync some number of steps
 *
 * param dir - direction to move motors
 * param count - number of steps to take
 * param step_delay - delay inbetween step pulses (units: us)
 */
void step_step(dir_t dir, uint32_t count, int step_delay) {

    // Set direction pins
    // TODO: test that enum value and accual direction of movement match
    if(dir == CLOSE) {
        digitalWrite(DIR_PIN_1, LOW);
        digitalWrite(DIR_PIN_2, LOW);
    }
    else if(dir == OPEN) {
        digitalWrite(DIR_PIN_1, HIGH);
        digitalWrite(DIR_PIN_2, HIGH);
    }

    // Step motors `count` number of times
    for(int i = 0; i < count; i++) {

        // Pulse step pins
        digitalWrite(STEP_PIN_1, HIGH);
        digitalWrite(STEP_PIN_2, HIGH);
        delayMicroseconds(STEP_PULSE_LEN);
        digitalWrite(STEP_PIN_1, LOW);
        digitalWrite(STEP_PIN_2, LOW);

        // Delay inbetween steps
        delayMicroseconds(step_delay);
    }
}

