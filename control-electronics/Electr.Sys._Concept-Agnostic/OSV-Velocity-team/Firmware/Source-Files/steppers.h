/**
 * steppers.h
 *
 * Author: Corbin Bremmeyr
 * Date: 9 April 2020
 *
 * Functions to interact with the stepper motor driver chips
 */

#ifndef STEPPERS_H
#define STEPPERS_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    CLOSE = 0,
    OPEN  = 1
} dir_t;


/**
 * Prepare the stepper library for use
 */
void step_init(void);

/**
 * Move both the stepper motors in sync some number of steps
 *
 * param dir - direction to move motors
 * param count - number of steps to take
 * param step_delay - delay inbetween step pulses (units: us)
 */
void step_step(dir_t dir, uint32_t count, int step_delay);

#ifdef __cplusplus
}
#endif

#endif

