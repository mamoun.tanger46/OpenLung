- ![](SM_of_OSV.svg)

# OSV State Machine

This model presents a simple proposal for OSV application layer section implemented entirely with UML state machines and the event-driven paradigm.for more information refer to [state-machine.com](https://www.state-machine.com)

## explanation

model consists of two independent variable of "on" and "ready" state.

### "on" state

This state is mainly responsible for system when ventilator is working. By entring this state (E in diagram)

1. Motor is turned on
2. A timer is enabled to read sensor priodically and notify GUI to updates Values and charts(if available)
3. It notifies GUI to change main page to show working page(update data lively)

By exiting this state(X in diagram)

1. Motor is turned off
2. Timer is disabled
3. It notifies GUI to change main page to show normal page(show default settings)

As well as handling above Event this state handles some other signals such as events come from buttons pressed by user(for example changing desired volume or pressure )

This State is container for three substates "pc","vc" and "alarm" which respectively corresponds to three mode of Pressure control,Volume Control and alarm. so by entring to this state based on situation one of these substate will be final state and control system. each one of this substate can handle other different events or overrides events handles by its superstate (in this case "on" state).If it can not handle a specific event it will be send to its superstate so it will handle it or else it will send to a higher level.
Events can be handled in state without transition to another state or it may makes transition to an arbitrary state.

#### substate "pc"

If system choose to be working in pressure control it wil be working in this state.

#### substate "vc"

If system choose to be working in volume control it wil be working in this state.

#### substate "alarm"

If by any chance there is a problem while system working it will transient to this state. For the the simplicity it is better events related to hardware problem only handled by "on" state and not overrided by "pc" and "vc" state and just problem related to respiratory parameter handled by these two substate(for exmple PROBLEM1 and PROBLEM2 in model). Current model proposes that using "alarm" substate inside "on" state because firing alarm while system is not used on patient is not reasonable.

### "ready" state

By entring this state system start hardware checking if it finds defects it transients to failure state or else system stays in this state until by an event transient to working state (in current case by START event coming from start pushbutton by user)

### "failure" state

If there is a problem with hardware system transient to this state. in this state it is better to show error to user in GUI and if it is possible to log error somewhere. there is no way to get out of this state unless to reset.

### GUI

The weird part of this design is GUI. GUI seems to be an independent (orthogonal) state compared to other state. It can be merged into these states but from S.O.L.I.D principle point of view it is not a good idea. So I think we better make an state for GUI but instead of treating it as a transible state, it will be invoked synchronously by other state event handles. It is somehow merging it while keeping it separated from other state.
